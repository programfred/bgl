<?php

use Illuminate\Support\Facades\Route;
use App\Models\Bike;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::post('/', function (Request $request) {
    $commandStr = $request->input('command');
    $commandArr= explode(" ", $commandStr);
    $bike = new Bike();
    $gps=null;
    switch( $commandArr[0]){
        case "PLACE":
            $param=explode(",", $commandArr[1]);
            $bike->place($param[0],$param[1],$param[2]);
            break;
        case "FORWARD":
            $bike->forward();
            break;
        case "TURN_LEFT":
            $bike->turnLeft();
            break;
        case "TURN_RIGHT":
            $bike->turnRight();
            break;
        case "GPS_REPORT":
            $gps=$bike->gpsReport();
            break;
         default:
           echo "invalid command";

    }

   


    // $bike->place(1, 1, "EAST");
    // $bike->forward();
    // $bike->gpsReport();
    
    return view('welcome', ['bikeLocation'=>$bike->getXY(),'facing'=>$bike->getFacing(),'gps'=>$gps]);
});
